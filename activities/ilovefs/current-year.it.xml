<?xml version="1.0" encoding="UTF-8"?>

<data>
  <version>2</version>
  <module>

    <h2>Giornata "Io ♥ il Software Libero" 2022</h2>

    <p>
    La giornata "Io amo il Software Libero" di quest'anno è dedicata ai 
    giochi rilasciati come Software Libero. I giochi, e specialmente 
    quelli di Software Libero, non sono solo un'attività divertente, ma 
    sono una parte importante della nostra vita quotidiana. Se da una 
    parte giocare può essere l'occasione di incontro di amici nuovi e 
    vecchi, dall'altra può rilassare e si possono imparare nuove 
    abilità, ma la cosa più importante è che puoi contribuire al tuo 
    gioco di Software Libero preferito e adattarlo come desideri. La 
    FSFE sta quindi pianificando uno speciale evento dedicato ai giochi 
    di Software Libero. Avremo tre esperti in giochi rilasciati come 
    Software Libero, motori di gioco e sessioni di gioco.</p>
    
    <p>Il <a href="https://download.fsfe.org/campaigns/ilovefs/games/Games%20Event/IloveFS22%20Agenda.pdf">agenda dell'evento</a> si presenta così:
     <ul>
      <li>18:00 - 18:05 (CET): Benvenuto e introduzione</li>
      <li>18:05 - 18:25 (CET): "Flare - Free/Libre Action Roleplaying Engine" di Justin Jacobs</li>
      <li>18:25 - 18:45 (CET): "Vassal – Free Software Game Engine" di Joel Uckelman</li>
      <li>18:45 - 19:05 (CET): "Godot Wild Jams" di Kati Baker</li>
      <li>19:05 - 19:20 (CET): "Veloren –Free Software Game" di Forest Anderson</li>
      <li>19:20 - 20:00 (CET):  Tempo di gioco</li>
      <li>20:00 (CET): Adieu</li>
     </ul>
    Per ulteriori informazioni sull'evento, guarda la nostra <a href="/news/2022/news-20220201-01.html">notizia</a>.
    </p>

    <p>
    Ma vogliamo anche dire <strong>grazie</strong> a tutti coloro che 
    hanno contribuito a progetti di Software Libero. Sfruttiamo la 
    giornata "Io amo il Software Libero" per ringraziare e festeggiare 
    il nostro amore per il Software Libero.
    </p>

    <figure>
      <img
        src="https://pics.fsfe.org/uploads/medium/69395a3acc8cfb98fbb058d0b44a8f04.png"
        alt="Evento di gioco Software Libero, 14 febbraio 2022 18:00 - 20:00 CET" />
          <figcaption>
          Festeggiamo la giornata #ilovefs!
          </figcaption>
    </figure>

    <p>
    Per la giornata "Io amo il Software Libero" di quest'anno abbiamo 
    una nuova puntata del Software Freedom Podcast. Il nostro ospite per 
    questa <a href="/news/podcast/episode-13.html">puntata speciale del 
    Software Freedom Podcast</a> è Stanislas Dolcini. Insieme a Bonnie e 
    Stanislas parleremo del gioco rilasciato come Software Libero 
    "0.A.D: Imperi dominanti (Empires Ascendant)".
    </p>


    <h2>Prendi parte alla giornata "Io amo il Software Libero"</h2>

    <div class="icon-grid">
      <ul>
        <li>
        <img src="/graphics/icons/ilovefs-use.png" alt="ILoveFS heart 
        with 'use'"/> <div> <h3>Sfrutta l'occasione</h3> <p>In questa 
        fantastica giornata, cogli l'occasione per ringraziare le 
        persone che ti permettono di beneficiare della libertà del 
        software. Usa le nostre nuove <a 
        href="https://download.fsfe.org/campaigns/ilovefs/games/">immagini 
        "Io amo il Software Libero: evento di gioco"</a>, la nostra <a 
        href="/news/podcast/episode-13.html">puntata speciale del 
        Software Freedom Podcast</a>, il <a 
        href="https://download.fsfe.org/campaigns/ilovefs/share-pics/">condividi-foto</a>, 
        gli <a href="/contribute/spreadtheword#ilovefs">adesivi e i 
        palloncini</a>, gli <a 
        href="/activities/ilovefs/artwork/artwork.html">elementi 
        grafici</a> e i <a href="/order/order.html">prodotti</a> messi a 
        disposizione dalla FSFE per <em>#ilovefs</em>. </p> </div>
        </li>

        <li>
        <img src="/graphics/icons/ilovefs-study.png" alt="ILoveFS heart 
        with 'study'"/> <div> <h3>Pensaci</h3> <p>Prova a pensare quale 
        progetto di Software Libero hai utilizzando lungo l'anno passo. 
        Con quale gioco di Software Libero hai giocato con i tuoi amici 
        o da solo? In che modo hai potuto divertirti mentre o perché hai 
        utilizzato Software Libero? Se hai bisogno di un'ispirazione, 
        dai un'occhiata alle <a 
        href="/activities/ilovefs/whylovefs/gallery.html">dichiarazione 
        d'amore</a> di altri. </p> </div>
        </li>

        <li>
        <img src="/graphics/icons/ilovefs-share.png" alt="ILoveFS heart 
        with 'share'"/> <div> <h3>Condividi il tuo amore</h3> <p>Questa 
        è la parte più divertente! Abbiamo creato un <a 
        href="https://sharepic.fsfe.org/">generatore di 
        condividi-foto</a>, con il quale puoi creare semplicemente la 
        tua condividi-foto personalizzata e condividere la tua stima sui 
        social (<em>#ilovefs</em>), nei blog, con immagini e messaggi 
        video, o direttamente con sviluppatori e con chi contribuisce al 
        Software Libero.</p> </div>
        </li>

        <li>
        <img src="/graphics/icons/ilovefs-improve.png" alt="ILoveFS 
        heart with 'improve'"/> <div> <h3>Migliora il Software 
        Libero</h3> <p>Parlarne è bene, contribuire è meglio! Aiuta un 
        progetto di Software Libero nel codice, nella sua traduzione o 
        aiutando i suoi utenti. O, se puoi, fai una donazione ad una 
        organizzazione di Software Libero, <a 
        href="https://my.fsfe.org/donate">come la FSFE</a>, o a un altro 
        progetto di Software Libero. </p> </div>
        </li>
      </ul>
    </div>

    <figure class="no-border">
      <img
        src="https://pics.fsfe.org/uploads/small/f164c8517e78449fc8844a72347af490.png"
        alt="#ilovefs" />
    </figure>

    <p>
    Qual è il tuo contributo a questa giornata speciale e alle persone 
    che ti permettono di usare il Software Libero? Pensi di utilizzare i 
    nostri adesivi e palloncini? O scattarti una foto o un video con la 
    nostra nuova maglietta ILoveFS? E cosa ne dici di festeggiare la 
    libertà del software con i tuoi colleghi e amici in un raduno 
    aziendale o ad un evento pubblico? Qualsiasi cosa tu faccia, 
    mostrala al mondo usando il tag <em>#ilovefs</em>. E se hai domande, <a 
    href="/contact/">basta scriverci un'email</a>.
    </p>

    <p>
    Buona giornata <strong><span class="text-danger">"Io amo il Software 
    Libero"</span></strong> a tutti! ❤
    </p>

  </module>
</data>

